package com.lowes.retailorder.service;

import com.lowes.retailorder.kafka.producer.KafkaProducer;
import com.lowes.retailorder.repository.Order;
import com.lowes.retailorder.repository.Orders;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class OrderServiceTest {

    @Mock
    Orders ordersRepo;

    @Mock
    KafkaProducer producer;

    @InjectMocks
    OrderServiceImpl orderService;

    @Test
    public void publishOrderTest() {
        Order orderIn = new Order();
        orderIn.setStatus("PLACED");
        orderIn.setAddress("jakkur");
        orderIn.setProductId("123ba7cb8c874d31ab4c35e3");
        orderIn.setOrderDate(new Date());
        orderIn.setTotalCost(25000);
        orderIn.setUserId("62c50d0870ce883911f13153");

        Order order = new Order();
        order.setStatus("PLACED");
        order.setAddress("jakkur");
        order.setProductId("123ba7cb8c874d31ab4c35e3");
        order.setOrderDate(new Date());
        order.setTotalCost(25000);
        order.setUserId("62c50d0870ce883911f13153");
        order.setId("62c53cb7e91a7b0b136c2b84");

        doReturn(order).when(ordersRepo).save(any(Order.class));
        doNothing().when(producer).sendMessage(order);
        Order res = orderService.publishOrder(orderIn);
        verify(producer, times(1)).sendMessage(res);
        assertThat(res.getId()).isNotNull();
        assertThat(res.getStatus()).isEqualTo("PLACED");
    }

    @Test
    public void getOrderTest() {

        Order order = new Order();
        order.setStatus("PROCESSED");
        order.setAddress("jakkur");
        order.setProductId("123ba7cb8c874d31ab4c35e3");
        order.setOrderDate(new Date());
        order.setTotalCost(25000);
        order.setUserId("62c50d0870ce883911f13153");
        order.setId("62c53cb7e91a7b0b136c2b84");

        doReturn(Optional.of(order)).when(ordersRepo).findById(anyString());
        Order res = orderService.getOrderDetails(anyString());
        assertThat(res.getId()).isNotNull();
        assertThat(res.getStatus()).isEqualTo("PROCESSED");
    }

    @Test
    public void testFallBack() {

        Order res = orderService.getOrderDetailsFallback(anyString());
        assertThat(res.getId()).isNull();
        assertThat(res.getStatus()).isNull();
    }
}
