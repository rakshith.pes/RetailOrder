package com.lowes.retailorder.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.lowes.retailorder.repository.Order;
import com.lowes.retailorder.repository.Orders;
import com.lowes.retailorder.service.OrderService;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Date;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@WithMockUser
public class OrderControllerTest {

    @Autowired
    private MockMvc mvc;

    @Autowired
    ObjectMapper mapper;

    @MockBean
    OrderService orderService;

    @Test
    void testpublishOrderDetails() throws Exception {

        Order order = new Order();
        order.setStatus("PLACED");
        order.setAddress("jakkur");
        order.setProductId("123ba7cb8c874d31ab4c35e3");
        order.setOrderDate(new Date());
        order.setTotalCost(25000);
        order.setUserId("62c50d0870ce883911f13153");

        mvc.perform(post("/api/lowes/v1/order")
                        .content(mapper.writeValueAsString(order))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    void testGetOrderDetails_200() throws Exception {

        Order order = new Order();
        order.setStatus("PLACED");
        order.setAddress("jakkur");
        order.setProductId("123ba7cb8c874d31ab4c35e3");
        order.setOrderDate(new Date());
        order.setTotalCost(25000);
        order.setUserId("62c50d0870ce883911f13153");
        order.setId("62c53cb7e91a7b0b136c2b84");
        doReturn(order).when(orderService).getOrderDetails(anyString());
        mvc.perform(get("/api/lowes/v1/order")
                        .queryParam("orderId", "62c53cb7e91a7b0b136c2b84")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    void testGetOrderDetails_204() throws Exception {
        Order order = new Order();
        doReturn(order).when(orderService).getOrderDetails(anyString());
        mvc.perform(get("/api/lowes/v1/order")
                        .queryParam("orderId", "62c53cb7e91a7b0b136c2b84")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());
    }

    @Test
    void testGetOrderDetails_404() throws Exception {
        doReturn(null).when(orderService).getOrderDetails(anyString());
        mvc.perform(get("/api/lowes/v1/order")
                        .queryParam("orderId", "62c53cb7e91a7b0b136c4b11")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

}
