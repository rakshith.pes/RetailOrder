package com.lowes.retailorder.controller;

import com.lowes.retailorder.OrderNotFoundException;
import com.lowes.retailorder.repository.Order;
import com.lowes.retailorder.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
public class OrderController {

    @Autowired
    OrderService service;
    /**
     *
     * This is the API endpoint used to place the order
     * @body Order details of the order placed
     * @return order with order id from the DB
     * @author Rakshith
     * @since 1.0
     */
    @PostMapping("/api/v1/lowes/v1/order")
    public Order publishOrderDetails(@RequestBody Order order) {

        return service.publishOrder(order);
    }

    /**
     *
     * This is the API endpoint used to get order details of a single order
     * @param orderId Id of an order
     * @return order details of the orderId
     * @author Rakshith
     * @since 1.0
     */
    @GetMapping("/api/v1/lowes/v1/order")
    public ResponseEntity<Order> publishOrderDetails(@RequestParam String orderId) {

        Order order = service.getOrderDetails(orderId);
        if(order==null) {
            throw new OrderNotFoundException();
        }
        ResponseEntity<Order> orderResponse;
        if(order.getId()!=null) {
            orderResponse = new ResponseEntity<>(order, HttpStatus.OK);
        } else {
            orderResponse = new ResponseEntity<>(order, HttpStatus.NO_CONTENT);
        }
        return orderResponse;
    }
}
