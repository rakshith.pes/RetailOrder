package com.lowes.retailorder.repository;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Generated;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;
import java.util.UUID;

@Data
@Document("order")
@AllArgsConstructor
public class Order {

        public Order() {}
        @Id
        @Generated
        private String id;
        private String productId;
        private int totalCost;
        private String address;
        private String status;
        private Date orderDate;
        private String userId;
}
