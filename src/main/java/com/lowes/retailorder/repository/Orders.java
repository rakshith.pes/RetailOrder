package com.lowes.retailorder.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface Orders extends MongoRepository<Order, String> {

    @Override
    Optional<Order> findById(String uuid);

    @Override
    <S extends Order> S save(S order);
}
