package com.lowes.retailorder;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;

@SpringBootApplication
@EnableHystrix
public class RetailOrderApplication {

    public static void main(String[] args) {
        SpringApplication.run(RetailOrderApplication.class, args);
    }

}
