package com.lowes.retailorder.service;

import com.lowes.retailorder.kafka.consumer.OrderProcessor;
import com.lowes.retailorder.kafka.producer.KafkaProducer;
import com.lowes.retailorder.repository.Order;
import com.lowes.retailorder.repository.Orders;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

@Service
@AllArgsConstructor
public class OrderServiceImpl implements OrderService{

    private static Logger logger = LoggerFactory.getLogger(OrderProcessor.class);
    private Orders orders;

    @Autowired
    KafkaProducer producer;

    /**
     *
     * Method to save the order details and also push the
     * message to kafka topic for processing
     * @param order
     * @return order details of the orderId
     * @author Rakshith
     * @since 1.0
     */
    @Override
    @Transactional
    public Order publishOrder(Order order) {

        Order response = (Order)orders.save(order);
        logger.info("Order saved, publishing to kafka topic");
        producer.sendMessage(response);
        return response;
    }

    /**
     *
     * Method to get the details of an order
     * message to kafka topic for processing
     * @param id of an order
     * @return Order object with details
     * @author Rakshith
     * @since 1.0
     */
    @Override
    @HystrixCommand(fallbackMethod = "getOrderDetailsFallback")
    public Order getOrderDetails(String id) {
        Order order = (Order)orders.findById(id).get();
        return order;
    }

    /**
     *
     * Fallback method for getOrderDetails
     * @param id orderId of an order
     * @return Order empty order object
     * @author Rakshith
     * @since 1.0
     */
    public Order getOrderDetailsFallback(String id) {

        return new Order();
    }
}
