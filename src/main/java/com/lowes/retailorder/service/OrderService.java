package com.lowes.retailorder.service;

import com.lowes.retailorder.repository.Order;

import java.util.UUID;

public interface OrderService {

    Order publishOrder(Order order);

    Order getOrderDetails(String id);
}
