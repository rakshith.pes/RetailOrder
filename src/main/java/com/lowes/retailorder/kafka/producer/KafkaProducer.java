package com.lowes.retailorder.kafka.producer;

import com.lowes.retailorder.kafka.consumer.OrderProcessor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class KafkaProducer<T> {

    private static Logger logger = LoggerFactory.getLogger(OrderProcessor.class);
    @Autowired
    private KafkaTemplate<String, T> kafkaTemplate;

    /**
     *
     * Kafka Producer
     * This will push the message to the order topic
     * @param message that contains order object payload
     * @author Rakshith
     * @since 1.0
     */
    public void sendMessage(T message) {
        logger.info("Order published to kafka");
        this.kafkaTemplate.send("order", message);
    }
}
