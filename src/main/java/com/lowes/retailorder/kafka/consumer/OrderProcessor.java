package com.lowes.retailorder.kafka.consumer;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.lowes.retailorder.repository.Order;
import com.lowes.retailorder.repository.Orders;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.DltHandler;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.annotation.RetryableTopic;
import org.springframework.kafka.retrytopic.DltStrategy;
import org.springframework.messaging.Message;
import org.springframework.retry.annotation.Backoff;
import org.springframework.stereotype.Service;

@Service
public class OrderProcessor {

    private static Logger logger = LoggerFactory.getLogger(OrderProcessor.class);
    @Autowired
    Orders orders;

    /**
     *
     * Kafka Consumer
     * This method will consume the Order from order topic and processes the same
     * @param message that contains order object payload
     * @author Rakshith
     * @since 1.0
     */
    @RetryableTopic(attempts = "2", exclude = {JsonProcessingException.class}, dltStrategy = DltStrategy.FAIL_ON_ERROR,
            backoff = @Backoff(delay = 10000, maxDelay = 60000, multiplier = 2))
    @KafkaListener(topics = "order", containerFactory = "kafkaListenerContainerFactory")
    void listener(Message<?> message) throws JsonProcessingException {
        logger.info("Order is getting processed");
        processor(message, "PROCESSED");
    }

    /**
     *
     * In case of multiple failures during message consumption
     * message will be pushed to DeadLetter Topic and will be processed accordingly
     * @param message that contains order object payload
     * @author Rakshith
     * @since 1.0
     */
    @DltHandler
    void Dltlistener(Message<?> message) throws JsonProcessingException {
        logger.info("processing the message from DLT");
        processor(message, "ORDER FAILED");
    }

    private void processor(Message<?> message, String status) throws JsonProcessingException{
        String payload = (String)message.getPayload();
        Order order = new ObjectMapper().readValue(payload, Order.class);
        order.setStatus(status);
        orders.save(order);
    }
}
