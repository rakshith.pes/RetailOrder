package com.lowes.retailorder;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 *
 * Exception Handler
 * 404 status message will be sent when the order is not found
 * @author Rakshith
 * @since 1.0
 */
@ResponseStatus(value= HttpStatus.NOT_FOUND, reason="No such Order")
public class OrderNotFoundException extends RuntimeException{

}
